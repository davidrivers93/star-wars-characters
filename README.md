# StarWars

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Structure

The project has to main views:
*  A main view where all the characters are listed using pagination.
*  A character view where a certain character data is showed like his weight, his height or the films hi has acted.

On shared we could shared components and services that are used on global application. It has two services:

*  Star wars service that talks with swapi api.
*  Character storage that saves to local storage the latest characters that the user has found.

## Development server

Run `npm install` for installing all the project dependencies locaally.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

For avoiding cors problems, on local environment we use a proxy that is located on [https://cors-anywhere.herokuapp.com/](https://cors-anywhere.herokuapp.com/) 

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Deployment process

As the source code is on gitlab, we use the gitlab runners for making all the CI/CD process.

On feature branches we run unit test on each commit is made.
On master branch, we build with `--prod` and `--aot` tags the project and after that we deploy to our S3 bucket the app.