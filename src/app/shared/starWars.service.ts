import { environment } from './../../environments/environment';
import { map } from 'rxjs/operators';
import { Character } from './../models/character.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StarWarsService {

  constructor(private http: HttpClient) { }

  public getCharacters(page?:number) : Observable<any> {
    let url = ""
    if(page){
      url = `${environment.base_url}people/?page=${page}`
    }else{
      url = `${environment.base_url}people/`
    }
    
    return this.http.get<any>(url);
  }

  public getCharacter(id):Observable<Character>{
    let character = environment.base_url +  `people/${id}`;
    return this.http.get<any>(character)
  }

  public getSpecie(id){
    let specie = environment.base_url +  `species/${id}`;
    return this.http.get<any>(specie)
  }

  public getFilm(id){
    let specie = environment.base_url +  `films/${id}`;
    return this.http.get<any>(specie)
  }

}