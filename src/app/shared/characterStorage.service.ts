import { Character } from './../models/character.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CharacterStorageService {

    orderedItems:BehaviorSubject<any> = new BehaviorSubject<any>([]);

    constructor() { 
        this.orderedItems.next(this.storageItems().reverse().slice(0,4));
    }

    setItem(id:Number,char:Character){
        let items:Array<any> = this.storageItems()?this.storageItems():[];
        if(!items.map(item => item.name).find((item) => item.toLowerCase() === char.name.toLowerCase())){
            items.push({name:char.name, id: id});
        }
        this.saveData(items);
    }

    private saveData(data:Array<String>){
        localStorage.setItem("latestCharacters", JSON.stringify(data));
        this.orderedItems.next(data.reverse().slice(0,4));
    }

    private storageItems(){
        return localStorage.getItem("latestCharacters")?JSON.parse(localStorage.getItem("latestCharacters")):[];
    }
}