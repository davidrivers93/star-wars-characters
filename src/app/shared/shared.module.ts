import { CharacterStorageService } from './characterStorage.service';
import { SpecieComponent } from './specie/specie.component';
import { NgModule } from '@angular/core';
import { StarWarsService } from './starWars.service';
import {  HttpClientModule } from '@angular/common/http';
import { FilmComponent } from './film/film.component';


@NgModule({
    imports: [
        HttpClientModule
    ],
    exports: [
        SpecieComponent,
        FilmComponent
    ],
    declarations: [
        SpecieComponent,
        FilmComponent
    ],
    providers: [
        StarWarsService,
        CharacterStorageService
    ]
})
export class SharedModule { }
