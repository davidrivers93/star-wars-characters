import { StarWarsService } from './../starWars.service';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TestBed, async, tick, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { SpecieComponent } from './specie.component';
import { specieResponse } from './specie.response';

describe('Specie Component', () => {
    let starWarsService: StarWarsService;
    let component: SpecieComponent;
    let fixture:ComponentFixture<SpecieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        PaginationModule.forRoot(),
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        SpecieComponent
      ],
      providers:[
          StarWarsService,
        ]
    }).compileComponents();
    fixture = TestBed.createComponent(SpecieComponent);
    component = fixture.componentInstance;
    starWarsService = TestBed.get(StarWarsService);
    spyOn(starWarsService, 'getCharacter').and.returnValue(of(specieResponse));
    component.specie = "https://swapi.co/api/species/1/";
    component.ngOnChanges();
    fixture.detectChanges();
  }));

  it(`Expect to be a human`, () => {
    expect(component.data.name).toEqual("Human");
  });

});
