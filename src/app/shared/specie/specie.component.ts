import { Specie } from './../../models/specie.model';
import { StarWarsService } from './../starWars.service';
import { Component, OnChanges, Input } from '@angular/core';

@Component({
    selector: 'specie',
    templateUrl: 'specie.template.html'
})

export class SpecieComponent implements OnChanges {

    @Input() specie:string;

    data:Specie = new Specie();

    constructor(private starWarsService:StarWarsService) { }

    ngOnChanges() {
        let specieId = this.specie.split("/")[this.specie.split("/").length -2];
        this.starWarsService.getSpecie(specieId).subscribe((res) => this.data = res);
    }
}