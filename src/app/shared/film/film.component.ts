import { StarWarsService } from './../starWars.service';
import { Component, OnChanges, Input } from '@angular/core';
import { Film } from 'src/app/models/film.model';

@Component({
    selector: 'film',
    templateUrl: 'film.template.html'
})

export class FilmComponent implements OnChanges {

    @Input() film:string;

    data:Film = new Film();

    constructor(private starWarsService:StarWarsService) { }

    ngOnChanges() {
        let filmId = this.film.split("/")[this.film.split("/").length -2];
        this.starWarsService.getFilm(filmId).subscribe((res) => this.data = res);
    }
}