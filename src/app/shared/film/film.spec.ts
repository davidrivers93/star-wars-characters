import { StarWarsService } from './../starWars.service';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TestBed, async, tick, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { FilmComponent } from './film.component';
import { filmResponse } from './film.response';

describe('Film Component', () => {
    let starWarsService: StarWarsService;
    let component: FilmComponent;
    let fixture:ComponentFixture<FilmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        PaginationModule.forRoot(),
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        FilmComponent
      ],
      providers:[
          StarWarsService,
        ]
    }).compileComponents();
    fixture = TestBed.createComponent(FilmComponent);
    component = fixture.componentInstance;
    starWarsService = TestBed.get(StarWarsService);
    spyOn(starWarsService, 'getFilm').and.returnValue(of(filmResponse));
    component.film = "https://swapi.co/api/films/1/";
    component.ngOnChanges();
    fixture.detectChanges();
  }));

  it(`Expect to be a human`, () => {
    expect(component.data.title).toEqual("Return of the Jedi");
  });

});
