import { StarWarsService } from './../shared/starWars.service';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TestBed, async, tick, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CharactersComponent } from './characters.component';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { response } from './characters.response';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

describe('CharactersComponent', () => {
    let starWarsService: StarWarsService;
    let component: CharactersComponent;
    let fixture:ComponentFixture<CharactersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        PaginationModule.forRoot(),
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        CharactersComponent
      ],
      providers:[
          StarWarsService
        ]
    }).compileComponents();
    fixture = TestBed.createComponent(CharactersComponent);
    component = fixture.componentInstance;

    starWarsService = TestBed.get(StarWarsService);
    spyOn(starWarsService, 'getCharacters').and.returnValue(of(response));
    component.ngOnInit();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`Current page should be 1'`, () => {
    expect(component.currentPage).toEqual(1);
  });

  it(`Characters length should be 10`, fakeAsync(() => {
    tick();
    fixture.detectChanges();
    expect(component.characters.length).toEqual(10);
  }));

  it(`Characters length should be 10`, fakeAsync(() => {
    component.pageChanged({page:2});
    tick();
    fixture.detectChanges();
    expect(component.characters.length).toEqual(10);
  }));
  
});
