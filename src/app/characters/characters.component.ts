import { Router } from '@angular/router';
import { Character } from './../models/character.model';
import { StarWarsService } from './../shared/starWars.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'characters',
    templateUrl: 'characters.template.html'
})

export class CharactersComponent implements OnInit {
    characters:Character[] = [];
    currentPage:number = 1;
    totalItems:number = 64;

    constructor(private starWarsService:StarWarsService,
                private router:Router) { 


    }

    ngOnInit() { 
        this.starWarsService.getCharacters().subscribe((res) => {
            this.totalItems = res.count;
            this.characters = res.results.map((item) => new Character(item));
        });
        
    }

    pageChanged(event){
        this.starWarsService.getCharacters(event.page).subscribe((res) => {
            this.totalItems = res.count;
            this.characters = res.results.map((item) => new Character(item));
        });
    }

    goToCharacter(character:Character){
        this.router.navigate(["character", character.getCharacterId()]);
    }
}