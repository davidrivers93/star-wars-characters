import { CharacterStorageService } from './../../shared/characterStorage.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'navbar',
    templateUrl: 'navbar.template.html'
})

export class NavbarComponent{
    latestCharacters:Array<any>;

    constructor(private characterStorageService:CharacterStorageService) {
        this.characterStorageService.orderedItems.subscribe((items) => {
            console.log(items);
            this.latestCharacters = items;
        });
    }
}