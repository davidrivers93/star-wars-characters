//Was generated using the JSON to TS extension for JS
interface FilmInterface {
    title:string//-- The title of this film
    episode_id:Number //-- The episode number of this film.
    opening_crawl:string //-- The opening paragraphs at the beginning of this film.
    director:string //-- The name of the director of this film.
    producer:string //-- The name(s) of the producer(s) of this film. Comma separated.
    release_date:Date //-- The ISO 8601 date format of film release at original creator country.
    species:Array<string> //-- An array of species resource URLs that are in this film.
    starships:Array<string> //-- An array of starship resource URLs that are in this film.
    vehicles:Array<string> //-- An array of vehicle resource URLs that are in this film.
    characters:Array<string> //-- An array of people resource URLs that are in this film.
    planets:Array<string> //-- An array of planet resource URLs that are in this film.
    url:string //-- the hypermedia URL of this resource.
    created:string //-- the ISO 8601 date format of the time that this resource was created.
}

export class Film implements FilmInterface {
    title:string//-- The title of this film
    episode_id:Number //-- The episode number of this film.
    opening_crawl:string //-- The opening paragraphs at the beginning of this film.
    director:string //-- The name of the director of this film.
    producer:string //-- The name(s) of the producer(s) of this film. Comma separated.
    release_date:Date //-- The ISO 8601 date format of film release at original creator country.
    species:Array<string> //-- An array of species resource URLs that are in this film.
    starships:Array<string> //-- An array of starship resource URLs that are in this film.
    vehicles:Array<string> //-- An array of vehicle resource URLs that are in this film.
    characters:Array<string> //-- An array of people resource URLs that are in this film.
    planets:Array<string> //-- An array of planet resource URLs that are in this film.
    url:string //-- the hypermedia URL of this resource.
    created:string //-- the ISO 8601 date format of the time that this resource was created.

    constructor(instance?: FilmInterface) {
        Object.assign(this, instance);
    }
}
