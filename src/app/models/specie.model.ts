//Was generated using the JSON to TS extension for JS
interface SpecieInterface {
    name:String //-- The name of this species.
    classification:String //-- The classification of this species, such as "mammal" or "reptile".
    designation:String //-- The designation of this species, such as "sentient".
    average_height:String //-- The average height of this species in centimeters.
    average_lifespan:String //-- The average lifespan of this species in years.
    eye_colors:String //-- A comma-separated string of common eye colors for this species, "none" if this species does not typically have eyes.
    hair_colors:String //-- A comma-separated string of common hair colors for this species, "none" if this species does not typically have hair.
    skin_colors:String //-- A comma-separated string of common skin colors for this species, "none" if this species does not typically have skin.
    language:String //-- The language commonly spoken by this species.
    homeworld:String //-- The URL of a planet resource, a planet that this species originates from.
    people:Array<String> //-- An array of People URL Resources that are a part of this species.
    films:Array<String> //-- An array of Film URL Resources that this species has appeared in.
    url:String //-- the hypermedia URL of this resource.
    created:String //-- the ISO 8601 date format of the time that this resource was created.
    edited:String //-- the ISO 8601 date format of the time that this resource was edited.
}

export class Specie implements SpecieInterface {
    name:String //-- The name of this species.
    classification:String //-- The classification of this species, such as "mammal" or "reptile".
    designation:String //-- The designation of this species, such as "sentient".
    average_height:String //-- The average height of this species in centimeters.
    average_lifespan:String //-- The average lifespan of this species in years.
    eye_colors:String //-- A comma-separated string of common eye colors for this species, "none" if this species does not typically have eyes.
    hair_colors:String //-- A comma-separated string of common hair colors for this species, "none" if this species does not typically have hair.
    skin_colors:String //-- A comma-separated string of common skin colors for this species, "none" if this species does not typically have skin.
    language:String //-- The language commonly spoken by this species.
    homeworld:String //-- The URL of a planet resource, a planet that this species originates from.
    people:Array<String> //-- An array of People URL Resources that are a part of this species.
    films:Array<String> //-- An array of Film URL Resources that this species has appeared in.
    url:String //-- the hypermedia URL of this resource.
    created:String //-- the ISO 8601 date format of the time that this resource was created.
    edited:String //-- the ISO 8601 date format of the time that this resource was edited.
    
    constructor(instance?: SpecieInterface) {
        Object.assign(this, instance);
    }
}
