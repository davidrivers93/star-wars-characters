//Was generated using the JSON to TS extension for JS
interface CharacterInterface {
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string[];
    species: Array<String>;
    vehicles: string[];
    starships: string[];
    created: string;
    edited: string;
    url: string;
}

export class Character implements CharacterInterface {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: Array<String> = [];
  vehicles: string[];
  starships: string[];
  created: string;
  edited: string;
  url: string;

  constructor(instance?: CharacterInterface) {
      Object.assign(this, instance);
  }

  getCharacterId(){
    return this.url.split("/")[this.url.split("/").length-2];
  }

  getImageUrl(){
    if(this.getSpecie()){
      return `/assets/images/${this.getSpecie().replace(" ","")}.png`
    }else{
      return null;
    }
  }
  
  getSpecie(){
    if(!this.species.length){
      return null;
    }
    const mainSpecie = this.species[0].split("/")[this.species[0].split("/").length-2];
    if(mainSpecie === "1"){
      return "human"
    }
    return "non human";
  }
}
