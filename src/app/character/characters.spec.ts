import { CharacterStorageService } from './../shared/characterStorage.service';
import { ActivatedRouteStub } from './../testing/activate-route-stub';
import { SharedModule } from './../shared/shared.module';
import { StarWarsService } from './../shared/starWars.service';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TestBed, async, tick, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { CharacterComponent } from './character.component';

import { responseCharacter } from './character.response';
import { ActivatedRoute, Params } from '@angular/router';

describe('Character Component', () => {
    let starWarsService: StarWarsService;
    let component: CharacterComponent;
    let fixture:ComponentFixture<CharacterComponent>;
    let activatedRoute:ActivatedRouteStub = new ActivatedRouteStub({ id: 1 });
    let characterStorageService: CharacterStorageService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        PaginationModule.forRoot(),
        FormsModule,
        HttpClientModule,
        SharedModule
      ],
      declarations: [
        CharacterComponent
      ],
      providers:[
          StarWarsService,
          CharacterStorageService,
          {
            provide: ActivatedRoute,
            useValue: {
                params: {
                    subscribe: (fn: (value: Params) => void) => fn({
                        id: 1,
                    }),
                },
            },
          }
        ]
    }).compileComponents();


    characterStorageService = TestBed.get(CharacterStorageService);
    activatedRoute.setParamMap({ id: 1 });
    fixture = TestBed.createComponent(CharacterComponent);
    component = fixture.componentInstance;
    starWarsService = TestBed.get(StarWarsService);
    spyOn(starWarsService, 'getCharacter').and.returnValue(of(responseCharacter));
    component.ngOnInit();

  }));
  // it('should create the component', () => {
  //   expect(component).toBeTruthy();
  // });

  it(`Expect to be Luke`, () => {
    expect(component.character.name).toEqual("Luke Skywalker");
  });

});
