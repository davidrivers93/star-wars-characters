import { CharacterStorageService } from './../shared/characterStorage.service';
import { Character } from './../models/character.model';
import { StarWarsService } from './../shared/starWars.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'character',
    templateUrl: 'character.template.html'
})

export class CharacterComponent implements OnInit {

    character:Character = new Character();

    constructor(private route:ActivatedRoute,
                private starWarsService:StarWarsService,
                private characterStorageService:CharacterStorageService) {
                    
        this.route.params.subscribe(params => {
            starWarsService.getCharacter(params['id']).subscribe((res) => {
                this.character = new Character(res);
                this.characterStorageService.setItem(params['id'],res);
            });
        });
     }

    ngOnInit() { }
}