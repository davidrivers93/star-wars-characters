import { CharactersComponent } from './characters/characters.component';
import { LayoutComponent } from './layout/layout.component';
import { CharacterComponent } from './character/character.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { 
        path: '',
        component: CharactersComponent
      },
      { 
        path: 'character/:id',
        component: CharacterComponent
      }
    ]
  },
  // Not found
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
